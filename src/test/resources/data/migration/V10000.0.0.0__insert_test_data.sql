insert into lecturer(full_name, phone, email, birthday, user_id, academic_degree_id)
values ('bachelor bachelor bachelor', '+7(777)777-77-77', 'bachelor@test.test', '2021-2-25', 1, 1);
insert into lecturer(full_name, phone, email, birthday, user_id, academic_degree_id)
values ('master master master', '+7(777)777-77-88', 'master@test.test', '2021-2-25', 2, 2);
insert into lecturer(full_name, phone, email, birthday, user_id, academic_degree_id)
values ('phd phd phd', '+7(777)777-77-99', 'phd@test.test', '2021-2-25', 3, 3);
insert into lecturer(full_name, phone, email, birthday, user_id, academic_degree_id)
values ('test for unassigned course', 'test', 'test', now(), 4, 1);

insert into lecturer_course(lecturer_id, course_id) values (2, 1);
insert into lecturer_course(lecturer_id, course_id) values (4, 1)