package kz.zhanbolat.lecturer;

public class TestConstant {
    public static final long NOT_EXISTING_LECTURER_ID = 100L;
    public static final long COURSE_ID = 1L;
    public static final LecturerModel BACHELOR_LECTURER = new LecturerModel(1L, 1L);
    public static final LecturerModel MASTER_LECTURER = new LecturerModel(2L, 2L);
    public static final LecturerModel PHD_LECTURER = new LecturerModel(3L, 3L);
    public static final long LECTURER_WITH_COURSE = MASTER_LECTURER.getId();
    public static final long ASSIGNED_LECTURER_ID = 4L;

    public static class LecturerModel {
        private final long id;
        private final long userId;

        private LecturerModel(long id, long userId) {
            this.id = id;
            this.userId = userId;
        }

        public long getId() {
            return id;
        }

        public long getUserId() {
            return userId;
        }
    }
}
