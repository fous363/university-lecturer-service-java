package kz.zhanbolat.lecturer;

import kz.zhanbolat.lecturer.entity.Lecturer;
import kz.zhanbolat.lecturer.exception.LecturerNotFoundException;
import kz.zhanbolat.lecturer.service.LecturerService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;

import static kz.zhanbolat.lecturer.TestConstant.*;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(classes = TestConfiguration.class)
@ActiveProfiles("test")
public class LecturerServiceTest {
    @Autowired
    public LecturerService lecturerService;
    @Autowired
    public Function<Long, Lecturer> lecturerGenerator;

    @Test
    @DisplayName("GET LECTURER BY USER ID CASE")
    public void givenNull_whenGetLecturerByUserId_thenThrowException() {
        assertThrows(Exception.class, () -> lecturerService.getLecturerByUserId(null));
    }

    @Test
    @DisplayName("GET LECTURER BY USER ID CASE")
    public void givenNotExistingUserId_whenGetLecturerByUserId_thenThrowException() {
        assertThrows(LecturerNotFoundException.class, () -> lecturerService.getLecturerByUserId(NOT_EXISTING_LECTURER_ID));
    }

    @Test
    @DisplayName("GET LECTURER BY USER ID CASE")
    public void givenExistingUserId_whenGetLecturerByUserId_thenReturnLecturer() {
        final Lecturer lecturer = lecturerService.getLecturerByUserId(BACHELOR_LECTURER.getUserId());

        assertEquals(1L, lecturer.getUserId());
        assertEquals(1L, lecturer.getId());
        assertEquals("bachelor bachelor bachelor", lecturer.getFullName());
        assertEquals("+7(777)777-77-77", lecturer.getPhone());
        assertEquals("bachelor@test.test", lecturer.getEmail());
        assertEquals(LocalDate.of(2021, 2, 25), lecturer.getBirthday());
        assertEquals(1, lecturer.getAcademicDegreeId());
    }

    @Test
    @DisplayName("ADD LECTURER CASE")
    public void givenNull_whenAddLecturer_thenThrowException() {
        assertThrows(Exception.class, () -> lecturerService.addLecturer(null));
    }

    @Test
    @DisplayName("ADD LECTURER CASE")
    public void givenLecturer_whenAddLecturer_thenReturnLecturerWithId() {
        Lecturer lecturer = new Lecturer();
        lecturer.setFullName("test");
        lecturer.setPhone("test");
        lecturer.setEmail("test");
        lecturer.setBirthday(LocalDate.now());
        lecturer.setUserId(10L);
        lecturer.setAcademicDegreeId(1);

        Lecturer createdLecturer = lecturerService.addLecturer(lecturer);
        assertNotNull(createdLecturer.getId());
        assertEquals(lecturer.getFullName(), createdLecturer.getFullName());
        assertEquals(lecturer.getBirthday(), createdLecturer.getBirthday());
        assertEquals(lecturer.getPhone(), createdLecturer.getPhone());
        assertEquals(lecturer.getEmail(), createdLecturer.getEmail());
        assertEquals(lecturer.getAcademicDegreeId(), createdLecturer.getAcademicDegreeId());
    }

    @Test
    @DisplayName("GET ASSIGNED TO COURSE LECTURERS CASE")
    public void givenNull_whenGetAssignedLecturersCourse_thenThrowException() {
        assertThrows(Exception.class, () -> lecturerService.getAssignedLecturersCourse(null));
    }

    @Test
    @DisplayName("GET ASSIGNED TO COURSE LECTURERS CASE")
    public void givenCourseId_whenGetAssignedLecturersCourse_thenReturnNotEmptyList() {
        List<Lecturer> lecturers = lecturerService.getAssignedLecturersCourse(COURSE_ID);

        assertNotNull(lecturers);
        assertFalse(lecturers.isEmpty());
    }

    @Test
    @DisplayName("GET UNASSIGNED TO COURSE LECTURERS CASE")
    public void givenNull_whenGetUnassignedLecturersCourse_thenThrowException() {
        assertThrows(Exception.class, () -> lecturerService.getUnassignedLecturersCourse(null));
    }

    @Test
    @DisplayName("GET UNASSIGNED TO COURSE LECTURERS CASE")
    public void givenCourseId_whenGetUnassignedLecturersCourse_thenReturnNotEmptyList() {
        List<Lecturer> lecturers = lecturerService.getUnassignedLecturersCourse(COURSE_ID);

        assertNotNull(lecturers);
        assertFalse(lecturers.isEmpty());
    }

    @Test
    @DisplayName("ASSIGN LECTURERS TO COURSE CASE")
    public void givenNull_whenAssignLecturerCourseList_thenThrowException() {
        assertAll(() -> {
            assertThrows(Exception.class, () -> lecturerService.assignLecturerCourseList(null, null));
            assertThrows(Exception.class, () -> lecturerService.assignLecturerCourseList(1L, null));
            assertThrows(Exception.class, () -> lecturerService.assignLecturerCourseList(null, Collections.emptyList()));
        });
    }

    @Test
    @DisplayName("ASSIGN LECTURERS TO COURSE CASE")
    public void givenEmptyList_whenAssignLecturerCourseList_thenThrowException() {
        assertThrows(IllegalArgumentException.class, () -> lecturerService.assignLecturerCourseList(COURSE_ID, Collections.emptyList()));
    }

    @Test
    @DisplayName("ASSIGN LECTURERS TO COURSE CASE")
    public void givenAlreadyAssignedLecturer_whenAssignLecturerCourseList_thenThrowException() {
        List<Lecturer> lecturers = Arrays.asList(lecturerGenerator.apply(BACHELOR_LECTURER.getId()),
                lecturerGenerator.apply(LECTURER_WITH_COURSE));
        assertThrows(IllegalArgumentException.class, () -> lecturerService.assignLecturerCourseList(COURSE_ID, lecturers));
    }

    @Test
    @DisplayName("ASSIGN LECTURERS TO COURSE CASE")
    public void givenLecturersAndCourse_whenAssignLecturerCourseList_thenDoesNotThrowException() {
        assertDoesNotThrow(() -> lecturerService.assignLecturerCourseList(COURSE_ID,
                Collections.singletonList(lecturerGenerator.apply(BACHELOR_LECTURER.getId()))));
    }

    @Test
    @DisplayName("UNASSIGN LECTURER TO COURSE CASE")
    public void givenNull_whenUnassignLecturerCourse_thenThrowException() {
        assertAll(() -> {
            assertThrows(Exception.class, () -> lecturerService.unassignLecturerCourse(null, null));
            assertThrows(Exception.class, () -> lecturerService.unassignLecturerCourse(1L, null));
            assertThrows(Exception.class, () -> lecturerService.unassignLecturerCourse(null, 1L));
        });
    }

    @Test
    @DisplayName("UNASSIGN LECTURER TO COURSE CASE")
    public void givenUnassignedLecturer_whenUnassignLecturerCourse_thenThrowException() {
        assertThrows(IllegalArgumentException.class, () -> lecturerService.unassignLecturerCourse(PHD_LECTURER.getId(), COURSE_ID));
    }

    @Test
    @DisplayName("UNASSIGN LECTURER TO COURSE CASE")
    public void givenAssignedLecturer_whenUnassignLecturerCourse_thenDoesNotThrowException() {
        assertDoesNotThrow(() -> lecturerService.unassignLecturerCourse(ASSIGNED_LECTURER_ID, COURSE_ID));
    }

    @Test
    @DisplayName("GET LECTURER CASE")
    public void givenNull_whenGetLecturer_thenThrowException() {
        assertThrows(Exception.class, () -> lecturerService.getLecturer(null));
    }

    @Test
    @DisplayName("GET LECTURER CASE")
    public void givenNotExistingLecturerId_whenGetLecturer_thenThrowException() {
        assertThrows(IllegalArgumentException.class, () -> lecturerService.getLecturer(NOT_EXISTING_LECTURER_ID));
    }

    @Test
    @DisplayName("GET LECTURER CASE")
    public void givenLecturerId_whenGetLecturer_thenReturnLecturer() {
        Lecturer lecturer = lecturerService.getLecturer(BACHELOR_LECTURER.getId());

        assertNotNull(lecturer);
        assertEquals(BACHELOR_LECTURER.getId(), lecturer.getId());
    }

    @Test
    @DisplayName("GET LECTURERS CASE")
    public void givenRequest_whenGetLecturers_thenReturnNotEmptyList() {
        List<Lecturer> lecturers = lecturerService.getLecturers();

        assertNotNull(lecturers);
        assertFalse(lecturers.isEmpty());
    }

    @Test
    @DisplayName("GET COURSES ID BY LECTURER ID CASE")
    public void givenNull_whenGetCourses_thenThrowException() {
        assertThrows(Exception.class, () -> lecturerService.getCoursesId(null));
    }

    @Test
    @DisplayName("GET COURSES ID BY LECTURER ID CASE")
    public void givenLecturerId_whenGetCourses_thenReturnNotEmptyList() {
        List<Long> coursesId = lecturerService.getCoursesId(LECTURER_WITH_COURSE);

        assertNotNull(coursesId);
        assertFalse(coursesId.isEmpty());
    }
}
