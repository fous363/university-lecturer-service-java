package kz.zhanbolat.lecturer;

import com.fasterxml.jackson.databind.ObjectMapper;
import kz.zhanbolat.lecturer.entity.Lecturer;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;

import static kz.zhanbolat.lecturer.TestConstant.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = TestConfiguration.class)
@ActiveProfiles("test")
@AutoConfigureMockMvc
public class LecturerControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private Function<Long, Lecturer> lecturerGenerator;

    @Test
    @WithMockUser(username = "lecturer_user", authorities = {"LECTURER"})
    @DisplayName("GET LECTURER BY USER ID CASE")
    public void givenNotExistingUserId_whenGetLecturerByUserId_thenReturnErrorResponse() throws Exception {
        mockMvc.perform(get("/api/lecturer/user/" + NOT_EXISTING_LECTURER_ID))
                .andExpect(status().isInternalServerError())
                .andExpect(jsonPath("$.message").value("Internal server error."));
    }

    @Test
    @WithMockUser(username = "lecturer_user", authorities = {"LECTURER"})
    @DisplayName("GET LECTURER BY USER ID CASE")
    public void givenExistingUserId_whenGetLecturerByUserId_thenReturnLecturer() throws Exception {
        mockMvc.perform(get("/api/lecturer/user/" + BACHELOR_LECTURER.getUserId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.fullName").value("bachelor bachelor bachelor"))
                .andExpect(jsonPath("$.phone").value("+7(777)777-77-77"))
                .andExpect(jsonPath("$.email").value("bachelor@test.test"))
                .andExpect(jsonPath("$.userId").value(1))
                .andExpect(jsonPath("$.birthday").value(LocalDate.of(2021, 2, 25).toString()))
                .andExpect(jsonPath("$.academicDegreeId").value(1));
    }

    @Test
    @DisplayName("ADD LECTURER CASE")
    public void givenLecturer_whenAddLecturer_thenReturnLecturer() throws Exception {
        Lecturer lecturer = new Lecturer();
        lecturer.setFullName("test");
        lecturer.setEmail("test");
        lecturer.setPhone("test");
        lecturer.setUserId(10L);
        lecturer.setBirthday(LocalDate.now());
        lecturer.setAcademicDegreeId(1);

        ObjectMapper objectMapper = new ObjectMapper();

        mockMvc.perform(post("/api/lecturer/")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(lecturer)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").isNotEmpty())
                .andExpect(jsonPath("$.fullName").value(lecturer.getFullName()))
                .andExpect(jsonPath("$.phone").value(lecturer.getPhone()))
                .andExpect(jsonPath("$.email").value(lecturer.getEmail()))
                .andExpect(jsonPath("$.userId").value(lecturer.getUserId()))
                .andExpect(jsonPath("$.birthday").value(lecturer.getBirthday().toString()))
                .andExpect(jsonPath("$.academicDegreeId").value(lecturer.getAcademicDegreeId()));
    }

    @Test
    @DisplayName("GET ASSIGNED TO COURSE LECTURERS CASE")
    @WithMockUser(authorities = {"ADMIN"})
    public void givenCourseId_whenGetAssignedLecturersCourse_thenReturnList() throws Exception {
        mockMvc.perform(get("/api/lecturer/assigned/course/" + COURSE_ID))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray());
    }

    @Test
    @DisplayName("GET UNASSIGNED TO COURSE LECTURERS CASE")
    @WithMockUser(authorities = {"ADMIN"})
    public void givenCourseId_whenGetUnassignedLecturersCourse_thenReturnList() throws Exception {
        mockMvc.perform(get("/api/lecturer/unassigned/course/" + COURSE_ID))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray());
    }

    @Test
    @DisplayName("ASSIGN LECTURERS TO COURSE CASE")
    @WithMockUser(authorities = {"ADMIN"})
    public void givenAlreadyAssignedLecturer_whenAssignLecturerCourseList_thenReturnErrorResponse() throws Exception {
        String payload = objectMapper.writeValueAsString(Arrays.asList(lecturerGenerator.apply(BACHELOR_LECTURER.getId()),
                lecturerGenerator.apply(LECTURER_WITH_COURSE)));

        mockMvc.perform(post("/api/lecturer/assign/list/course/" + COURSE_ID)
                .contentType(MediaType.APPLICATION_JSON).content(payload))
                .andExpect(status().isInternalServerError())
                .andExpect(jsonPath("$.message").isNotEmpty());
    }

    @Test
    @DisplayName("ASSIGN LECTURERS TO COURSE CASE")
    @WithMockUser(authorities = {"ADMIN"})
    public void givenLecturersAndCourse_whenAssignLecturerCourseList_thenStatusOk() throws Exception {
        List<Lecturer> lecturers = Collections.singletonList(lecturerGenerator.apply(BACHELOR_LECTURER.getId()));
        String payload = objectMapper.writeValueAsString(lecturers);

        mockMvc.perform(post("/api/lecturer/assign/list/course/" + COURSE_ID)
                .contentType(MediaType.APPLICATION_JSON).content(payload))
                .andExpect(status().isOk());
    }

    @Test
    @DisplayName("UNASSIGN LECTURER TO COURSE CASE")
    @WithMockUser(authorities = {"ADMIN"})
    public void givenUnassignedLecturer_whenUnassignLecturerCourse_thenReturnErrorResponse() throws Exception {
        mockMvc.perform(post("/api/lecturer/" + PHD_LECTURER.getId() + "/unassign/course/" + COURSE_ID))
                .andExpect(status().isInternalServerError())
                .andExpect(jsonPath("$.message").isNotEmpty());
    }

    @Test
    @DisplayName("UNASSIGN LECTURER TO COURSE CASE")
    @WithMockUser(authorities = {"ADMIN"})
    public void givenAssignedLecturer_whenUnassignLecturerCourse_thenReturnStatusOk() throws Exception {
        mockMvc.perform(post("/api/lecturer/" + ASSIGNED_LECTURER_ID + "/unassign/course/" + COURSE_ID))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(authorities = "STUDENT")
    @DisplayName("GET LECTURER CASE")
    public void givenNotExistingLecturerId_whenGetLecturer_thenReturnErrorResponse() throws Exception {
        mockMvc.perform(get("/api/lecturer/" + NOT_EXISTING_LECTURER_ID))
                .andExpect(status().isInternalServerError())
                .andExpect(jsonPath("$.message").isNotEmpty());
    }

    @Test
    @WithMockUser(authorities = "STUDENT")
    @DisplayName("GET LECTURER CASE")
    public void givenLecturerId_whenGetLecturer_thenReturnLecturer() throws Exception {
        mockMvc.perform(get("/api/lecturer/" + BACHELOR_LECTURER.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(BACHELOR_LECTURER.getId()))
                .andExpect(jsonPath("$.fullName").isNotEmpty())
                .andExpect(jsonPath("$.phone").isNotEmpty())
                .andExpect(jsonPath("$.birthday").isNotEmpty())
                .andExpect(jsonPath("$.email").isNotEmpty())
                .andExpect(jsonPath("$.userId").isNotEmpty())
                .andExpect(jsonPath("$.academicDegreeId").isNotEmpty());
    }

    @Test
    @WithMockUser(authorities = "STUDENT")
    @DisplayName("GET LECTURERS CASE")
    public void givenRequest_whenGetLecturers_thenReturnList() throws Exception {
        mockMvc.perform(get("/api/lecturer"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray());
    }

    @Test
    @WithMockUser(authorities = "LECTURER")
    @DisplayName("GET COURSES ID BY LECTURER ID CASE")
    public void givenLecturerId_whenGetCoursesId_thenReturnList() throws Exception {
        mockMvc.perform(get("/api/lecturer/" + LECTURER_WITH_COURSE + "/courses/id"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$").isNotEmpty());
    }
}
