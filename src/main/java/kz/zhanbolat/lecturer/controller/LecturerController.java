package kz.zhanbolat.lecturer.controller;

import kz.zhanbolat.lecturer.controller.dto.ErrorResponse;
import kz.zhanbolat.lecturer.entity.Lecturer;
import kz.zhanbolat.lecturer.exception.LecturerNotFoundException;
import kz.zhanbolat.lecturer.service.LecturerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/lecturer")
public class LecturerController {
    private static final Logger logger = LoggerFactory.getLogger(LecturerController.class);
    @Autowired
    private LecturerService lecturerService;

    @GetMapping("/user/{userId}")
    public Lecturer getLecturerByUserId(@PathVariable("userId") Long userId) {
        return lecturerService.getLecturerByUserId(userId);
    }

    @PostMapping
    public Lecturer addLecturer(@RequestBody Lecturer lecturer) {
        return lecturerService.addLecturer(lecturer);
    }

    @GetMapping("/assigned/course/{courseId}")
    public List<Lecturer> getAssignedLecturersCourse(@PathVariable("courseId") Long courseId) {
        return lecturerService.getAssignedLecturersCourse(courseId);
    }

    @GetMapping("/unassigned/course/{courseId}")
    public List<Lecturer> getUnassignedLecturersCourse(@PathVariable("courseId") Long courseId) {
        return lecturerService.getUnassignedLecturersCourse(courseId);
    }

    @PostMapping("/assign/list/course/{courseId}")
    public ResponseEntity assignLecturerCourseList(@PathVariable("courseId") Long courseId,
                                                   @RequestBody List<Lecturer> lecturers) {
        lecturerService.assignLecturerCourseList(courseId, lecturers);
        return ResponseEntity.ok().build();
    }

    @PostMapping("/{id}/unassign/course/{courseId}")
    public ResponseEntity unassignLecturerCourse(@PathVariable("id") Long id, @PathVariable("courseId") Long courseId) {
        lecturerService.unassignLecturerCourse(id, courseId);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/{id}")
    public Lecturer getLecturer(@PathVariable("id") Long id) {
        return lecturerService.getLecturer(id);
    }

    @GetMapping
    public List<Lecturer> getLecturers() {
        return lecturerService.getLecturers();
    }

    @GetMapping("/{id}/courses/id")
    public List<Long> getCoursesId(@PathVariable("id") Long id) {
        return lecturerService.getCoursesId(id);
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler
    public ErrorResponse handleException(Exception exception) {
        logger.error("Caught exception", exception);
        return new ErrorResponse("Internal server error.");
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(LecturerNotFoundException.class)
    public ErrorResponse handleNotFoundException(Exception exception) {
        logger.error("Caught not found exception", exception);
        return new ErrorResponse("Internal server error.");
    }
}
