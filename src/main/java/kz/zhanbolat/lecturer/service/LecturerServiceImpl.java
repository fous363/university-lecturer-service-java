package kz.zhanbolat.lecturer.service;

import kz.zhanbolat.lecturer.entity.Lecturer;
import kz.zhanbolat.lecturer.exception.LecturerNotFoundException;
import kz.zhanbolat.lecturer.repository.LecturerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;

@Service
public class LecturerServiceImpl implements LecturerService {
    @Autowired
    private LecturerRepository lecturerRepository;

    @Override
    public Lecturer getLecturerByUserId(Long userId) {
        Objects.requireNonNull(userId, "User id cannot be null.");
        return lecturerRepository.findOneByUserId(userId)
                .orElseThrow(() -> new LecturerNotFoundException("Lecturer with user id " + userId + " is not found."));
    }

    @Override
    public Lecturer addLecturer(Lecturer lecturer) {
        Objects.requireNonNull(lecturer, "Lecturer cannot be null.");
        return lecturerRepository.save(lecturer);
    }

    @Override
    public List<Lecturer> getAssignedLecturersCourse(Long courseId) {
        Objects.requireNonNull(courseId, "Course id cannot be null");

        return lecturerRepository.findAllAssignedToCourse(courseId);
    }

    @Override
    public List<Lecturer> getUnassignedLecturersCourse(Long courseId) {
        Objects.requireNonNull(courseId, "Course id cannot be null.");

        return lecturerRepository.findAllUnassignedToCourse(courseId);
    }

    @Override
    @Transactional
    public void assignLecturerCourseList(Long courseId, List<Lecturer> lecturers) {
        Objects.requireNonNull(courseId, "Course id cannot be null.");
        Objects.requireNonNull(lecturers, "Lecturers list cannot be null.");
        if (lecturers.isEmpty()) {
            throw new IllegalArgumentException("Lecturers list cannot be empty.");
        }
        lecturers.stream()
                .filter(lecturer -> lecturerRepository.isCourseAlreadyAssignedToLecturer(lecturer.getId(), courseId))
                .findFirst().ifPresent(lecturer -> { throw new IllegalArgumentException("Course is already assigned to lecturer '"
                + lecturer.getFullName() + "'"); });

        lecturers.forEach(lecturer -> lecturerRepository.assignLecturerCourse(lecturer.getId(), courseId));
    }

    @Override
    @Transactional
    public void unassignLecturerCourse(Long id, Long courseId) {
        Objects.requireNonNull(id, "Id cannot be null.");
        Objects.requireNonNull(courseId, "Course id cannot be null.");

        if (!lecturerRepository.isCourseAlreadyAssignedToLecturer(id, courseId)) {
            throw new IllegalArgumentException("Course is unassigned to lecturer");
        }

        lecturerRepository.unassignLecturerCourse(id, courseId);
    }

    @Override
    public Lecturer getLecturer(Long id) {
        Objects.requireNonNull(id, "Id cannot be null.");

        return lecturerRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("No lecturer with id " + id));
    }

    @Override
    public List<Lecturer> getLecturers() {
        return (List<Lecturer>) lecturerRepository.findAll();
    }

    @Override
    public List<Long> getCoursesId(Long id) {
        Objects.requireNonNull(id, "Id cannot be null.");

        return lecturerRepository.findCoursesIdByLecturerId(id);
    }
}
