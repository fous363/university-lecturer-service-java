package kz.zhanbolat.lecturer.service;

import kz.zhanbolat.lecturer.entity.Lecturer;

import java.util.List;

public interface LecturerService {
    Lecturer getLecturerByUserId(Long userId);

    Lecturer addLecturer(Lecturer lecturer);

    List<Lecturer> getAssignedLecturersCourse(Long courseId);

    List<Lecturer> getUnassignedLecturersCourse(Long courseId);

    void assignLecturerCourseList(Long courseId, List<Lecturer> lecturers);

    void unassignLecturerCourse(Long id, Long courseId);

    Lecturer getLecturer(Long id);

    List<Lecturer> getLecturers();

    List<Long> getCoursesId(Long id);
}
