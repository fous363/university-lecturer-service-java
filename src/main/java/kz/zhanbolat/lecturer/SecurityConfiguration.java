package kz.zhanbolat.lecturer;

import kz.zhanbolat.lecturer.controller.filter.AuthorizationFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

@Configuration
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
    @Autowired
    private AuthorizationFilter authorizationFilter;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .formLogin().disable()
                .logout().disable()
                .csrf().disable()
                .authorizeRequests(authorizeRequestsCustomizer -> authorizeRequestsCustomizer
                        .antMatchers(HttpMethod.GET, "/api/lecturer/user/*",
                                "/api/lecturer/*/courses/id").hasAuthority("LECTURER")
                        .antMatchers(HttpMethod.POST, "/api/lecturer/").permitAll()
                        .antMatchers(HttpMethod.GET, "/api/lecturer/assigned/course/*",
                                "/api/lecturer/unassigned/course/*").hasAuthority("ADMIN")
                        .antMatchers(HttpMethod.POST, "/api/lecturer/assign/list/course/*",
                                "/api/lecturer/*/unassign/course/*").hasAuthority("ADMIN")
                        .antMatchers(HttpMethod.GET, "/api/lecturer/*", "/api/lecturer")
                            .hasAnyAuthority("STUDENT", "LECTURER", "ADMIN")
                        .and()
                        .addFilterBefore(jwtAuthorizationFilterBean().getFilter(), UsernamePasswordAuthenticationFilter.class))
                .cors();
    }

    @Bean
    public FilterRegistrationBean jwtAuthorizationFilterBean() {
        FilterRegistrationBean registration = new FilterRegistrationBean();
        registration.setFilter(authorizationFilter);
        registration.addUrlPatterns("/api/lecturer**");
        registration.setOrder(Ordered.HIGHEST_PRECEDENCE);
        return registration;
    }
}
