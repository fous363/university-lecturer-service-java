package kz.zhanbolat.lecturer.repository;

import kz.zhanbolat.lecturer.entity.Lecturer;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface LecturerRepository extends CrudRepository<Lecturer, Long> {
    Optional<Lecturer> findOneByUserId(Long userId);

    @Query(value = "select case when count(*) > 0 then true else false end " +
            "from lecturer_course where lecturer_id = ?1 and course_id = ?2", nativeQuery = true)
    boolean isCourseAlreadyAssignedToLecturer(Long lecturerId, Long courseId);

    @Modifying
    @Query(value = "insert into lecturer_course(lecturer_id, course_id) values (?1, ?2)", nativeQuery = true)
    void assignLecturerCourse(Long lecturerId, Long courseId);

    @Query(value = "select l.id, l.full_name, l.phone, l.email, l.birthday, l.user_id, l.academic_degree_id " +
            "from lecturer l inner join lecturer_course lc on lc.lecturer_id = l.id and lc.course_id = ?1",
            nativeQuery = true)
    List<Lecturer> findAllAssignedToCourse(Long courseId);

    @Query(value = "select l.id, l.full_name, l.phone, l.email, l.birthday, l.user_id, l.academic_degree_id " +
            "from lecturer l where l.id not in (select lc.lecturer_id from lecturer_course lc where lc.course_id = ?1)",
            nativeQuery = true)
    List<Lecturer> findAllUnassignedToCourse(Long courseId);

    @Modifying
    @Query(value = "delete from lecturer_course where lecturer_id = ?1 and course_id = ?2", nativeQuery = true)
    void unassignLecturerCourse(Long id, Long courseId);

    @Query(value = "select course_id from lecturer_course where lecturer_id = ?1", nativeQuery = true)
    List<Long> findCoursesIdByLecturerId(Long id);
}
