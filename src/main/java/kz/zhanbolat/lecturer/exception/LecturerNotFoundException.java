package kz.zhanbolat.lecturer.exception;

public class LecturerNotFoundException extends RuntimeException {

    public LecturerNotFoundException(String message) {
        super(message);
    }
}
