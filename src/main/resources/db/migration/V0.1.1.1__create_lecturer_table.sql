create table if not exists academic_degree(
    id smallserial primary key,
    name varchar(100) not null
);

create table if not exists lecturer(
    id bigserial primary key,
    full_name varchar(200) not null,
    phone char(16) not null,
    email varchar(100) not null,
    user_id bigint not null unique,
    birthday date not null,
    academic_degree_id smallint not null,
    foreign key (academic_degree_id) references academic_degree(id)
);