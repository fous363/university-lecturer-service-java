alter table lecturer
drop constraint lecturer_academic_degree_id_fkey;

delete from academic_degree;

drop table academic_degree;