create table if not exists lecturer_course(
    lecturer_id bigint not null,
    course_id bigint not null,
    foreign key (lecturer_id) references lecturer(id)
);